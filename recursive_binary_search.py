nums = [1, 3, 5, 7, 9, 11]
target = 5


def find_index(nums, target, start=0, end=len(nums)):

    if start > end:
        return "Not found"

    middle = (start + end) // 2

    if nums[middle] == target:
        return f"Found index for {target}: {nums.index(target)}"

    if nums[middle] > target:
        return find_index(nums, target, start, end=middle - 1)

    if nums[middle] < target:
        return find_index(nums, target, start=middle + 1, end=end)


res = find_index(nums, target)
print(res)
